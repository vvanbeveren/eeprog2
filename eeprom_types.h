/**
 *  ------------------------------------------------------------------------------
 *
 *            NIKHEF - National Institute for Subatomic Physics
 *
 *                        Computer Technology Department
 *
 *  ----------------------------------------------------------------------------
 *  @file     eeprom_types.h
 *
 *  @author   Vincent van Beveren <v.van.beveren@nikhef.nl>
 *
 *  @version  1.0
 *
 *  @brief  	I2C EEPROM library part definitions
 *
 *  Why not simply use the generic types?
 *  The generic types all assume maximum page write time of 25 ms, and assume a specific page size. This page size has
 *  been chosen on the small side. This is quite safe, but on the other and, if the actual page size is 16 bytes, while
 *  the generic assumes 8 byte, it will require two program/erase cycles. This decreases the durability of the part.
 *
 *  So in general: Using generic parts are slower, and wear out the actual part more quickly.
 *
 *  @copyright 2019-2022 Nikhef Amsterdam
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SRC_EEPROM_TYPES_H_
#define SRC_EEPROM_TYPES_H_



//! Generic tiny 8 bit
extern const eeprom_info_t GENERIC_8BIT_TINY;

//! Generic 8 bit
extern const eeprom_info_t GENERIC_8BIT;

//! Generic 16 bit
extern const eeprom_info_t GENERIC_16BIT;

//! Microchip 24XX64 (24AA64, 24FC64, 24LC64)
extern const eeprom_info_t MICROCHIP_24XX64;

//! Microchip 24XX00 (24AA00, 24LC00, 24C00)
extern const eeprom_info_t MICROCHIP_24XX00;

//! Microchip 24XX01 8 byte page (24AA01, 24LC01B)
extern const eeprom_info_t MICROCHIP_24XX01;

//! Microchip 24XX01 16 byte page (24AA014, 24LC014)
extern const eeprom_info_t MICROCHIP_24XX014;

//! Microchip 24AA02x 8 byte page (24AA02x, 24LC02x, 24LV02x)
extern const eeprom_info_t MICROCHIP_24XX02;

//! Microchip 24AA024x 16 byte page (24AA024x, 24LC024x, 24LV024x)
extern const eeprom_info_t MICROCHIP_24XX024;

//! Microchip 24AA025x 16 byte page (24AA025x, 24LC025x, 24LV025x)
extern const eeprom_info_t MICROCHIP_24XX025;

//! Microchip 24XX04 (24AA04, 24LC04B)
extern const eeprom_info_t MICROCHIP_24XX04;

//! Microchip 24XX08 (24AA08, 24LC08B)
extern const eeprom_info_t MICROCHIP_24XX08;

//! Microchip 24XX16 (24AA16, 24LC16B)
extern const eeprom_info_t MICROCHIP_24XX16;

//! Microchip 24XX32 (24AA32A, 24LC32A)
extern const eeprom_info_t MICROCHIP_24XX32;

//! List of all EEPROMs, ending with a NULL pointer.
extern const eeprom_info_t * ALL_EEPROMS[];

#endif /* SRC_EEPROM_TYPES_H_ */
