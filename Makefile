all: eeprog2

clean: 
	rm -f eeprog2 *.o

eeprog2: eeprog2.o eeprom.o eeprom_types.o
	$(CC) $(LDFLAGS) -o $@ eeprog2.o eeprom.o eeprom_types.o $(LDLIBS)

