/**
 *  ------------------------------------------------------------------------------
 *
 *            NIKHEF - National Institute for Subatomic Physics
 *
 *                        Computer Technology Department
 *
 *  ----------------------------------------------------------------------------
 *  @file     eeprog2.c
 *
 *  @author   Vincent van Beveren <v.van.beveren@nikhef.nl>
 *
 *
 *  @version  1.0
 *
 *  @brief  	I2C EEPROM CLI
 *
 *
 *  @copyright 2019-2022 Nikhef Amsterdam
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <argp.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "eeprom.h"
#include "eeprom_types.h"

const char charopts[] = "o:s:lh";

static struct option long_options[] =
{
	{"off",  required_argument, 0, 'o'},
	{"size",  required_argument, 0, 's'},
//	{"hex",  no_argument, 0, 'x'},
	{"list",  no_argument, 0, 'l'},
	{"help",  no_argument, 0, 'h'},
	{0, 0, 0, 0}
};

void print_help()
{
	puts("eeprog2 [options] <type> <dev> <addr> <mode> [<file>] ");
	puts("  type               EEPROM type, use list option to see which are supported");
	puts("  dev                I2C device to use, for example /dev/i2c-0, /dev/i2c-1");
	puts("  addr               I2C address of EERPOM (0x2-0x7f), often 0x50");
	puts("  mode               Either r or p for Read (r) or program (p)");
	puts("  file               File to place read data in or read data from to program EEPROM");
	puts("                     When omitted uses stdin (program) or stdout (read)");
	puts("");
	puts("Options:");
	puts("  --off <n>,-o<n>    Offset to start read/write");
	puts("  --size <n>,-s<n>   Size to read or program. When not set for read size will be calculated.");
	puts("                     For EEPROMs g8 and g16 the size is 65536 bytes for 16 bits, and 256 bytes for 8 bit.");
	puts("  --list,-l          List all supported EEPROM types and exit");
	puts("  --help,-h          This help");
	puts("");
	puts("  All numbers may be in hex if prefixed with 0x, e.g. 0xeF");
}


void list_eeprom_types()
{
	const eeprom_info_t ** ptr = &ALL_EEPROMS[0];

	puts("The following EEPROM types are supported:");
	while (*ptr != NULL)
	{
		printf("  %-16s %s\n", (*ptr)->name, (*ptr)->descr);
		ptr++;
	}

}


const eeprom_info_t * get_eeprom_by_name(const char * name)
{
	const eeprom_info_t ** ptr = &ALL_EEPROMS[0];
	while (*ptr != NULL)
	{
		if (strcmp((*ptr)->name, name) == 0)
		{
			return *ptr;
		}
		ptr++;
	}
	return NULL;
}


bool get_number_option(int * result, const char * opt)
{
	// test for hexidecimal notation (i.e. 0xFF)
	if (strlen(opt) > 2 && opt[0] == '0' && opt[1] == 'x')
	{
		*result = (int)strtol(&opt[2], NULL, 16);
	}
	else
	{
		// otherwise treat as decimal
		*result = (int)strtol(opt, NULL, 10);
	}

	// set for conversion problem
	if (*result == 0 && errno == EINVAL)
	{
		printf("Could not parse '%s' as number\n", opt);
		return false;
	}

	return true;
}


eeprom_status_t exec_prog(uint8_t * buf, uint32_t size, uint16_t offset, eeprom_t * ee, FILE * fp)
{
	// first fill the buffer until EOF or size
	int c = getc(fp);
	int l = 0;
	while (c != EOF && l < size)
	{
		buf[l++] = c;
		c = getc(fp);
	}

	if (l == 0)
	{
		puts("Nothing to write (file empty?)");
		return EEPROM_ARG_ERROR;
	}

	printf("Writing %d byte(s) to EEPROM (max=%d)\n", l, size);
	return eeprom_prog(ee, offset, buf, l);
}


eeprom_status_t exec_read(uint8_t * buf, uint32_t size, uint16_t offset, eeprom_t * ee, FILE * fp)
{
	eeprom_status_t status = eeprom_read(ee, offset, buf, size);

	if (status != EEPROM_SUCCESS) return status;

	fwrite(buf, 1, size, fp);

	return EEPROM_SUCCESS;
}


int main(int argc, char *argv[])
{

	int offset = 0;
	int size = -1;
	int c;
	int option_index = 0;
	int addr;
	uint32_t eeprom_size;
	const char * dev;
	bool prog;
	const eeprom_info_t * type;

	// -----------------------------------------------------------------------------------------------------------------
	// Parse command-line arguments
	// -----------------------------------------------------------------------------------------------------------------

	while (1)
	{
		c = getopt_long (argc, argv, charopts,
		                 long_options, &option_index);

		if (c == -1) break;
		switch (c)
		{
		case 'h':	// help
			print_help();
			return 0;
		case 'o':	// offset
			if (!get_number_option(&offset, optarg))
			{
				print_help();
				return 1;
			}
			break;
		case 's':		// size
			if (!get_number_option(&size, optarg))
			{
				print_help();
				return 1;
			}
			break;
		case 'l':
			list_eeprom_types();
			return 0;
		case '?':		// some error
			/* no break */
		default:
			puts("Unknown or invalid option");
			print_help();
			return 1;
		}

	}

	// check for remaining arguments
	int remain = argc - optind;
	if (remain != 4 && remain != 5)
	{
		puts("Not enough or too many arguments");
		print_help();
		return 1;
	}

	// Find the EEPROM info
	type = get_eeprom_by_name(argv[optind++]);
	if (type == NULL)
	{
		printf("EEPROM with name '%s' is not found\n", argv[--optind]);
		return 1;
	}


	// get device
	dev = argv[optind++];

	// check I2C address
	if (!get_number_option(&addr, argv[optind++]))
	{
			return 1;
	}

	// get mode
	const char pr = argv[optind][0];

	if (strlen(argv[optind]) != 1 || (pr != 'p' && pr != 'r' ) )
	{
		puts("Expected either p or r");
    return 1;
	}
	prog = ( pr == 'p' );
	optind++;

	// -----------------------------------------------------------------------------------------------------------------
	// Opens stream for reading or writing
	// -----------------------------------------------------------------------------------------------------------------
	// calc size of EEPROM (for generics this may not be correct)
	eeprom_size = type->page * type->pages;

	// create buffer for read/prog
	// for reading we take the smallest buffer, for programming the largest
	if (size == -1) size = eeprom_size - offset;
	if (size > eeprom_size - offset) size = eeprom_size - offset;

	if (size <= 0)
	{
		puts("Offset exceeds size of EEPROM");
		return 1;
	}

	uint8_t buf[size];
	FILE * fp;

	if (optind < argc)
	{
		fp = fopen(argv[optind], prog ? "rb" : "wb");

		if (fp == NULL)
		{
			printf("Could not open file '%s'", argv[optind]);
		}
	}
	else
	{
		fp = stdout;
	}

	eeprom_t ee;
	eeprom_status_t status;

	status = eeprom_open(dev, &ee, addr, type);

	if (status != EEPROM_SUCCESS)
	{
		puts("EEPROM open failed");
		return 1;
	}

	// -----------------------------------------------------------------------------------------------------------------
	// Execute transfer
	// -----------------------------------------------------------------------------------------------------------------

	if (prog)
	{
		status = exec_prog(buf, size, offset, &ee, fp);
	}
	else
	{
		status = exec_read(buf, size, offset, &ee, fp);
	}

	if (status != EEPROM_SUCCESS)
	{
		puts("EEPROM transaction failed");
	}

	eeprom_close(&ee);

	if (fp != stdout) fclose(fp);

	return status == EEPROM_SUCCESS ? 0 : 1;
}
