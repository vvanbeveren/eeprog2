# eeprog2

EEPROG2 is an I2C EEPROM programmer tool to be used in (embedded) Linux environment.

Its conceptually based on EEPROG (https://github.com/tat/eeprog), but has some distinct advantages

 - Able to specifiy parts (but also generic 8 and 16 bit EEPROMs are supported)
 - Knows about page sizes for more efficient and durable programming
 - Knows about programming delays. As such it waits until the page programming is complete
 - Uses the generic Linux I2C interface, not the SMBus, and thus does not need to bypass SMBus requirements

## Compilation:

Just invoke make
 
## Usage

```
eeprog2 [options] <type> <dev> <addr> <mode> [<file>] 
  type               EEPROM type, use list option to see which are supported
  dev                I2C device to use, for example /dev/i2c-0, /dev/i2c-1
  addr               I2C address of EERPOM (0x2-0x7f), often 0x50
  mode               Either r or p for Read (r) or program (p)
  file               File to place read data in or read data from to program EEPROM
                     When omitted uses stdin (program) or stdout (read)

Options:
  --off <n>,-o<n>    Offset to start read/write
  --size <n>,-s<n>   Size to read or program. When not set for read size will be calculated.
                     For EEPROMs g8 and g16 the size is 65536 bytes for 16 bits, and 256 bytes for 8 bit.
  --list,-l          List all supported EEPROM types and exit
  --help,-h          This help
```

## Supported Chips

```
  g8t              Generic tiny 8 bit EEPROM (1+ byte page, max 256 bit)
  g8               Generic 8 bit EEPROM (8+ byte page, max 2 Kbit)
  g16              Generic 16 bit EEPROM (8+ byte page, max 512 Kbit)
  mc24xx00         Microchip 24XX00 (24AA00, 24LC00, 24C00)
  mc24xx01         Microchip 24XX01 8 byte page (24AA01, 24LC01B)
  mc24xx014        Microchip 24XX01 16 byte page (24AA014, 24LC014)
  mc24xx024        Microchip 24XX02 8 byte page (24AA02x, 24LC02x, 24LV02x)
  mc24xx025        Microchip 24XX024X 16 byte page (24AA024x, 24LC024x, 24LV024x)
  mc24xx025        Microchip 24XX025X 16 byte page (24AA025x, 24LC025x, 24LV025x)
  mc24xx04         Microchip 24XX04 (24AA04, 24LC04B)
  mc24xx08         Microchip 24XX08 (24AA08, 24LC08B)
  mc24xx16         Microchip 24XX16 (24AA16, 24LC16B)
  mc24xx32         Microchip 24XX32 (24AA32A, 24LC32A)
  mc24xx64         Microchip 24XX64 (24AA64, 24FC64, 24LC64)
```

## License

Copyright (c) 2019-2022 Nikhef Amsterdam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Author
Vincent van Beveren &lt;v.van.beveren@nikhef.nl&gt;

Feel free to send me bug fixes, improvements, additional part defintions, etc...

