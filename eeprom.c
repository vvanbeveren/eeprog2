/**
 *  ------------------------------------------------------------------------------
 *
 *            NIKHEF - National Institute for Subatomic Physics
 *
 *                        Computer Technology Department
 *
 *  ----------------------------------------------------------------------------
 *  @file     eeprom.c
 *
 *  @author   Vincent van Beveren <v.van.beveren@nikhef.nl>
 *
 *
 *  @version  1.0
 *
 *  @brief  	I2C EEPROM library header (read/write functions).
 *
 *
 *  @copyright 2019-2022 Nikhef Amsterdam
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "eeprom.h"

#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <unistd.h>

eeprom_status_t eeprom_open(const char * dev,
		eeprom_t * ee, uint8_t addr, const eeprom_info_t * info)
{
	if (dev == NULL) return EEPROM_ARG_ERROR;
	if (ee == NULL) return EEPROM_ARG_ERROR;
	if (addr == 0) return EEPROM_ARG_ERROR;
	if (info == NULL) return EEPROM_ARG_ERROR;


	ee->_fp = open(dev, O_RDWR);

	if (!ee->_fp) return EEPROM_IO_ERROR;

	ee->addr = addr;
	ee->info = info;

	return EEPROM_SUCCESS;
}

eeprom_status_t eeprom_close(eeprom_t * ee)
{
	if (ee->_fp < 0) return EEPROM_ARG_ERROR;
	close(ee->_fp);
	ee->_fp = -1;

	return EEPROM_SUCCESS;
}

static eeprom_status_t eeprom_prog_page(eeprom_t * ee,
		uint16_t offset, uint8_t * buf_ptr, uint16_t buf_len, uint16_t * written)
{
	// create write buffers
	uint16_t to_write = offset % ee->info->page;
	if (to_write == 0) to_write = ee->info->page;
	if (to_write > buf_len) to_write = buf_len;

	uint8_t 						buf[to_write + 2];
	uint8_t * 					p = &buf[0];

	// setup address
	if (ee->info->flags & EEPROM_FLAGS_16_BITS)
	{
		*p++ = 0xFF & (offset >> 8);
		*p++ = 0xFF & offset;
	}
	else
	{
		*p++ = 0xFF & offset;
	}

	*written = to_write;

	// copy data
	while (to_write-- > 0)
	{
		*p++ = *buf_ptr++;
	}

	// Make I2C transaction with a single message.
	struct i2c_rdwr_ioctl_data dta;
	struct i2c_msg msg;
	dta.nmsgs = 1;
	dta.msgs = &msg;

	msg.addr = ee->addr;
	msg.buf = &buf[0];
	msg.flags = 0;
	msg.len = *written + ( ee->info->flags & EEPROM_FLAGS_16_BITS ? 2 : 1 );

	// execute transaction
	int ret = ioctl(ee->_fp, I2C_RDWR, &dta);

	if (ret < 0) return EEPROM_IO_ERROR;

	// sleep for requested period of time
	useconds_t prog_delay = 100 * ee->info->prog_delay;

	usleep(prog_delay);

	return EEPROM_SUCCESS;
}

eeprom_status_t eeprom_prog(eeprom_t * ee, uint16_t offset, uint8_t * buf_ptr, uint16_t buf_len)
{
	if (ee == NULL) return EEPROM_ARG_ERROR;
	if (buf_ptr == NULL) return EEPROM_ARG_ERROR;

	uint16_t written;
	eeprom_status_t status;

	while (buf_len > 0)
	{
		status = eeprom_prog_page(ee, offset, buf_ptr, buf_len, &written);
		if (status != EEPROM_SUCCESS)
			return status;
		buf_len -= written;
		buf_ptr += written;
		offset += written;
	}
	return EEPROM_SUCCESS;
}

eeprom_status_t eeprom_read(eeprom_t * ee, uint16_t offset, uint8_t * buf_ptr, uint16_t buf_len)
{
	if (ee == NULL) return EEPROM_ARG_ERROR;
	if (buf_ptr == NULL) return EEPROM_ARG_ERROR;

	// a.f.a.i.k. we do not need to worry about page boundaries for reading.
	uint8_t memaddr[2];

	// Make I2C transaction with a single message.
	struct i2c_rdwr_ioctl_data dta;
	struct i2c_msg msg[2];
	dta.nmsgs = 2;
	dta.msgs = msg;

	msg[0].addr = ee->addr;
	msg[0].buf = &memaddr[0];
	msg[0].flags = 0;

	// setup address
	if (ee->info->flags & EEPROM_FLAGS_16_BITS)
	{
		memaddr[0] = 0xFF & (offset >> 8);
		memaddr[1] = 0xFF & offset;
		msg[0].len = 2;
	}
	else
	{
		memaddr[0] = 0xFF & offset;
		msg[0].len = 1;
	}

	// setup read buffer.
	msg[1].addr = ee->addr;
	msg[1].buf = buf_ptr;
	msg[1].flags = I2C_M_RD;
	msg[1].len = buf_len;

	// execute transaction
	int ret = ioctl(ee->_fp, I2C_RDWR, &dta);

	if (ret < 0) return EEPROM_IO_ERROR;

	return EEPROM_SUCCESS;
}
