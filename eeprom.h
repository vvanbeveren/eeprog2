/**
 *  ------------------------------------------------------------------------------
 *
 *            NIKHEF - National Institute for Subatomic Physics
 *
 *                        Computer Technology Department
 *
 *  ----------------------------------------------------------------------------
 *  @file     eeprom.h
 *
 *  @author   Vincent van Beveren <v.van.beveren@nikhef.nl>
 *
 *
 *  @version  1.0
 *
 *  @brief  	I2C EEPROM library header (read/write functions).
 *
 *
 *  @copyright 2019-2022 Nikhef Amsterdam
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SRC_EEPROM_H_
#define SRC_EEPROM_H_

#include <stdint.h>
#include <stdbool.h>

#define EEPROM_FLAGS_16_BITS		0x01		//!< EEPROM is 16 bits

/**
 * EEPROM status
 */
typedef enum
{
	EEPROM_SUCCESS = 0,		//!< Last operation completed succesfully
	EEPROM_IO_ERROR = 1,	//!< Last operation failed with an IO error
	EEPROM_ARG_ERROR = 2	//!< Invalid argument
} eeprom_status_t;

/**
 * Structure describing an EEPROM.
 */
typedef struct
{
	uint16_t page;				//!< Page size
	uint16_t pages;				//!< number of pages
	uint8_t  prog_delay;	//!< Page program delay in units of 100 us.
	uint8_t  flags;				//!< flags, for now its just '16 bit'
	const char * name;		//!< Name of this definition (short name)
	const char * descr;		//!< Description
} eeprom_info_t;



/**
 * Strucutre describing the EEPROM.
 */
typedef struct
{
	int _fp;										//!< File pointer
	const eeprom_info_t * info;	//!< EEPROM information object.
	uint8_t  addr;							//!< I2C address
} eeprom_t;

/**
 * Initialize the EEPROM with provided INFO.
 *
 * @param		dev								I2C device name, e.g. /dev/i2c-1
 * @param		ee								EEPROM structure to set
 * @param 	addr							I2C EERPOM address, e.g. 0x50, top 7 bits of first I2C cycle.
 * @param		info							A EEPROM info structure.
 *
 * @retval	EEPROM_SUCCESS		Opened successful
 * @retval	EEPROM_IO_ERROR		If the EEPROM could not be opened.
 */
eeprom_status_t eeprom_open(const char * dev,
		eeprom_t * eeprom, uint8_t addr, const eeprom_info_t * info);

/**
 * Close the EEPROM handle.
 *
 * @param		ee								Pointer to EEPROM structure
 *
 * @retval	EEPROM_SUCCESS		Close successful
 * @retval	EEPROM_IO_ERROR		If the EEPROM could not be closed due to IO issue.
 */
eeprom_status_t eeprom_close(eeprom_t * eeprom);

/**
 * EEPROM program operation.
 *
 * @param		ee								The EEPROM to use
 * @param		offset						The offset
 * @param		buf_ptr						Pointer to buffer
 * @param		buf_len						Length of the buffer to write.
 *
 * @retval EEPROM_SUCCESS			Opened successful
 * @retval EEPROM_IO_ERROR		If the EEPROM could be programmed due to IO exception
 */
eeprom_status_t eeprom_prog(eeprom_t * ee, uint16_t offset, uint8_t * buf_ptr, uint16_t buf_len);

/**
 * EEPROM read operation.
 *
 * @param		ee								The EEPROM to use
 * @param		offset						The offset
 * @param		buf_ptr						Pointer to buffer
 * @param		buf_len						Length of the buffer to fill.
 *
 * @retval EEPROM_SUCCESS			Opened successful
 * @retval EEPROM_IO_ERROR		If the EEPROM could be read due to IO exception
 */
eeprom_status_t eeprom_read(eeprom_t * ee, uint16_t offset, uint8_t * buf_ptr, uint16_t buf_len);


#endif /* SRC_EEPROM_H_ */
