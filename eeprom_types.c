/**
 *  ------------------------------------------------------------------------------
 *
 *            NIKHEF - National Institute for Subatomic Physics
 *
 *                        Computer Technology Department
 *
 *  ----------------------------------------------------------------------------
 *  @file     eeprom_types.c
 *
 *  @author   Vincent van Beveren <v.van.beveren@nikhef.nl>
 *
 *  @version  1.0
 *
 *  @brief  	I2C EEPROM library part definitions
 *
 *  @copyright 2019-2022 Nikhef Amsterdam
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "eeprom.h"
#include <stdlib.h>

const eeprom_info_t GENERIC_8BIT_TINY = {
		.page = 1,
		.pages = 32,
		.prog_delay = 255,
		.flags = 0,
		.name = "g8t",
		.descr = "Generic tiny 8 bit EEPROM (1+ byte page, max 256 bit)"
};


const eeprom_info_t GENERIC_8BIT = {
		.page = 8,
		.pages = 32,
		.prog_delay = 255,
		.flags = 0,
		.name = "g8",
		.descr = "Generic 8 bit EEPROM (8+ byte page, max 2 Kbit)"
};

const eeprom_info_t GENERIC_16BIT = {
		.page = 8,
		.pages = 8192,
		.prog_delay = 255,
		.flags = EEPROM_FLAGS_16_BITS,
		.name = "g16",
		.descr = "Generic 16 bit EEPROM (8+ byte page, max 512 Kbit)"
};

const eeprom_info_t MICROCHIP_24XX00 = {
		.page = 1,
		.pages = 16,
		.prog_delay = 40,
		.flags = 0,
		.name = "mc24xx00",
		.descr = "Microchip 24XX00 (24AA00, 24LC00, 24C00)"
};


const eeprom_info_t MICROCHIP_24XX01 = {
		.page = 8,
		.pages = 16,
		.prog_delay = 50,
		.flags = 0,
		.name = "mc24xx01",
		.descr = "Microchip 24XX01 8 byte page (24AA01, 24LC01B)"
};

const eeprom_info_t MICROCHIP_24XX014 = {
		.page = 16,
		.pages = 8,
		.prog_delay = 50,
		.flags = 0,
		.name = "mc24xx014",
		.descr = "Microchip 24XX01 16 byte page (24AA014, 24LC014)"
};


const eeprom_info_t MICROCHIP_24XX02 = {
		.page = 8 ,
		.pages = 32,
		.prog_delay = 50,
		.flags = 0,
		.name = "mc24xx02",
		.descr = "Microchip 24XX02 8 byte page (24AA02x, 24LC02x, 24LV02x)"
};




const eeprom_info_t MICROCHIP_24XX024 = {
		.page = 16 ,
		.pages = 16,
		.prog_delay = 50,
		.flags = 0,
		.name = "mc24xx024",
		.descr = "Microchip 24XX024X 16 byte page (24AA024x, 24LC024x, 24LV024x)"
};




const eeprom_info_t MICROCHIP_24XX025 = {
		.page = 16 ,
		.pages = 16,
		.prog_delay = 50,
		.flags = 0,
		.name = "mc24xx025",
		.descr = "Microchip 24XX025X 16 byte page (24AA025x, 24LC025x, 24LV025x)"
};




const eeprom_info_t MICROCHIP_24XX04 = {
		.page = 16 ,
		.pages = 32,
		.prog_delay = 50,
		.flags = EEPROM_FLAGS_16_BITS,
		.name = "mc24xx04",
		.descr = "Microchip 24XX04 (24AA04, 24LC04B)"
};

const eeprom_info_t MICROCHIP_24XX08 = {
		.page = 16 ,
		.pages = 64,
		.prog_delay = 50,
		.flags = EEPROM_FLAGS_16_BITS,
		.name = "mc24xx08",
		.descr = "Microchip 24XX08 (24AA08, 24LC08B)"
};

const eeprom_info_t MICROCHIP_24XX16 = {
		.page = 16 ,
		.pages = 128,
		.prog_delay = 50,
		.flags = EEPROM_FLAGS_16_BITS,
		.name = "mc24xx16",
		.descr = "Microchip 24XX16 (24AA16, 24LC16B)"
};

const eeprom_info_t MICROCHIP_24XX32 = {
		.page = 32 ,
		.pages = 128,
		.prog_delay = 50,
		.flags = EEPROM_FLAGS_16_BITS,
		.name = "mc24xx32",
		.descr = "Microchip 24XX32 (24AA32A, 24LC32A)"
};

const eeprom_info_t MICROCHIP_24XX64 = {
		.page = 32 ,
		.pages = 256,
		.prog_delay = 50,
		.flags = EEPROM_FLAGS_16_BITS,
		.name = "mc24xx64",
		.descr = "Microchip 24XX64 (24AA64, 24FC64, 24LC64)"
};

const eeprom_info_t * ALL_EEPROMS[] =
{
		&GENERIC_8BIT_TINY,
		&GENERIC_8BIT,
		&GENERIC_16BIT,
		&MICROCHIP_24XX00,
		&MICROCHIP_24XX01,
		&MICROCHIP_24XX014,
		&MICROCHIP_24XX02,
		&MICROCHIP_24XX024,
		&MICROCHIP_24XX025,
		&MICROCHIP_24XX04,
		&MICROCHIP_24XX08,
		&MICROCHIP_24XX16,
		&MICROCHIP_24XX32,
		&MICROCHIP_24XX64,
		NULL
};
